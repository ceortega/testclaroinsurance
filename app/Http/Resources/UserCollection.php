<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;


class UserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'draw' => intval($request->query('draw')),
            'recordsTotal' => $this->collection->count(),
            'recordsFiltered' => $this->collection->count(),
            'data' => $this->collection,
        ];
    }
}
