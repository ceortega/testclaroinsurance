<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     * Se recomienda ejecutar seeds manualmente en el orden descrito en metodo call.
     * 
     * @return void
     */
    public function run()
    {
        $this->call(
            CountriesTableSeeder::class,
            StatesTableSeeder::class,
            CitiesTableSeeder::class,
            UsersTableSeeder::class
        );
    }
}
