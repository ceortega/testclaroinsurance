<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**CIUDADES VINCULADAS A LAS PROVINCIAS DE ECUADOR */
        DB::table('cities')->insert([
            ['name' => 'CHORDELEG', 'state_id' => 1],
            ['name' => 'CUENCA', 'state_id' => 1],
            ['name' => 'EL PAN', 'state_id' => 1],
            ['name' => 'GIRON', 'state_id' => 1],
            ['name' => 'GUACHAPALA', 'state_id' => 1],
            ['name' => 'GUALACEO', 'state_id' => 1],
            ['name' => 'NABON', 'state_id' => 1],
            ['name' => 'ONIA', 'state_id' => 1],
            ['name' => 'PAUTE', 'state_id' => 1],
            ['name' => 'PONCE ENRIQUEZ', 'state_id' => 1],
            ['name' => 'PUCARA', 'state_id' => 1],
            ['name' => 'SAN FERNANDO', 'state_id' => 1],
            ['name' => 'SAN FERNANDO', 'state_id' => 1],
            ['name' => 'SANTA ISABEL', 'state_id' => 1],
            ['name' => 'SEVILLA DE ORO', 'state_id' => 1],
            ['name' => 'SIGSIG', 'state_id' => 1],
            ['name' => 'CALUMA', 'state_id' => 2],
            ['name' => 'CHILLANES', 'state_id' => 2],
            ['name' => 'CHIMBO', 'state_id' => 2],
            ['name' => 'ECHEANDIA', 'state_id' => 2],
            ['name' => 'GUARANDA', 'state_id' => 2],
            ['name' => 'LAS NAVES', 'state_id' => 2],
            ['name' => 'SAN MIGUEL', 'state_id' => 2],
            ['name' => 'AZOGUES', 'state_id' => 3],
            ['name' => 'BIBLIAN', 'state_id' => 3],
            ['name' => 'CANIAR', 'state_id' => 3],
            ['name' => 'DELEG', 'state_id' => 3],
            ['name' => 'EL TAMBO', 'state_id' => 3],
            ['name' => 'LA TRONCAL', 'state_id' => 3],
            ['name' => 'SUSCAL', 'state_id' => 3],
            ['name' => 'BOLIVAR - CARCHI', 'state_id' => 4],
            ['name' => 'ESPEJO', 'state_id' => 4],
            ['name' => 'HUACA', 'state_id' => 4],
            ['name' => 'MONTUFAR', 'state_id' => 4],
            ['name' => 'MIRA', 'state_id' => 4],
            ['name' => 'TULCAN', 'state_id' => 4],
            ['name' => 'LA MANA', 'state_id' => 5],
            ['name' => 'LATACUNGA', 'state_id' => 5],
            ['name' => 'PANGUA', 'state_id' => 5],
            ['name' => 'PUJILI', 'state_id' => 5],
            ['name' => 'SALCEDO', 'state_id' => 5],
            ['name' => 'SAQUISILI', 'state_id' => 5],
            ['name' => 'SIGCHOS', 'state_id' => 5],
            ['name' => 'ALAUSI', 'state_id' => 6],
            ['name' => 'COLTA', 'state_id' => 6],
            ['name' => 'CUMANDA', 'state_id' => 6],
            ['name' => 'CHAMBO', 'state_id' => 6],
            ['name' => 'CHUNCHI', 'state_id' => 6],
            ['name' => 'GUAMOTE', 'state_id' => 6],
            ['name' => 'GUANO', 'state_id' => 6],
            ['name' => 'PALLATANGA', 'state_id' => 6],
            ['name' => 'PENIPE', 'state_id' => 6],
            ['name' => 'RIOBAMBA', 'state_id' => 6],
            ['name' => 'ARENILLAS', 'state_id' => 7],
            ['name' => 'ATAHUALPA', 'state_id' => 7],
            ['name' => 'BALSAS', 'state_id' => 7],
            ['name' => 'CHILLA', 'state_id' => 7],
            ['name' => 'EL GUABO', 'state_id' => 7],
            ['name' => 'HUAQUILLAS', 'state_id' => 7],
            ['name' => 'LAS LAJAS', 'state_id' => 7],
            ['name' => 'MACHALA', 'state_id' => 7],
            ['name' => 'MARCABELI', 'state_id' => 7],
            ['name' => 'PASAJE', 'state_id' => 7],
            ['name' => 'PINIAS', 'state_id' => 7],
            ['name' => 'PORTOVELO', 'state_id' => 7],
            ['name' => 'SANTA ROSA', 'state_id' => 7],
            ['name' => 'ZARUMA', 'state_id' => 7],
        ]);
    }
}