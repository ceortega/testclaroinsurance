<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'cedula' => '01234567891',
            'name' => 'admin',
            'fecha' => '2021-12-12',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin@gmail.com'),
            'city_id' => 1
        ]);
    }
}
