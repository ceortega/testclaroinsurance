<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert([
            /**ESTADOS/PROVINCIAS DE ECUADOR */
            ['name' => 'AZUAY', 'country_id' => 1],
            ['name' => 'BOLIVAR', 'country_id' => 1],
            ['name' => 'CANIAR', 'country_id' => 1],
            ['name' => 'CARCHI', 'country_id' => 1],
            ['name' => 'COTOPAXI', 'country_id' => 1],
            ['name' => 'CHIMBORAZO', 'country_id' => 1],
            ['name' => 'EL ORO', 'country_id' => 1],
            ['name' => 'ESMERALDAS', 'country_id' => 1],
            ['name' => 'GUAYAS', 'country_id' => 1],
            ['name' => 'IMBABURA', 'country_id' => 1],
            ['name' => 'LOJA', 'country_id' => 1],
            ['name' => 'LOS RIOS', 'country_id' => 1],
            ['name' => 'MANABI', 'country_id' => 1],
            ['name' => 'MORONA SANTIAGO', 'country_id' => 1],
            ['name' => 'NAPO', 'country_id' => 1],
            ['name' => 'PASTAZA', 'country_id' => 1],
            ['name' => 'PICHINCHA', 'country_id' => 1],
            ['name' => 'TUNGURAHUA', 'country_id' => 1],
            ['name' => 'ZAMORA CHINCHIPE', 'country_id' => 1],
            ['name' => 'GALAPAGOS', 'country_id' => 1],
            ['name' => 'SUCUMBIOS', 'country_id' => 1],
            ['name' => 'ORELLANA', 'country_id' => 1],
            ['name' => 'SANTO DOMINGO DE LOS TSACHILAS', 'country_id' => 1],
            ['name' => 'SANTA ELENA', 'country_id' => 1],
        ]);
    }
}
